using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEditor.SearchService;
using UnityEngine;

public class BallPlacing : MonoBehaviour
{
    private int BallCount = 0;
    private bool PlayerInZone;

    private void Update()
    {
        if (PlayerInZone && Input.GetKeyDown(KeyCode.E))
            {
                CountBall();
            }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerInZone = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerInZone = false;
        }
    }

    void CountBall()
    {
        GameObject[] balls = GameObject.FindGameObjectsWithTag("Collect");

        BallCount = balls.Length;

        Debug.Log("Liczba przedmiotów z tagiem 'Collect' na scenie: " + BallCount);
    }
}