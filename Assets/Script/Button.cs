using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Button : MonoBehaviour
{
    private GameObject button;
    private bool PlayerInZone;
    private bool Click;
    private Animator animator;
    public GameObject key;

    // Update is called once per frame
    void Start()
    {
        PlayerInZone = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && PlayerInZone == true) 
        {
            GetComponent<Animator>().Play("click");
            key.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerInZone=true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        { 
            PlayerInZone=false; 
        }
    }

    
}
