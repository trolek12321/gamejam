using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pick : MonoBehaviour
{
    private bool PlayerInZone;

    private void Start()
    {
        PlayerInZone = false;
    }
    private void Update()
    {
        if (PlayerInZone == true && Input.GetKeyDown(KeyCode.E)) 
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerInZone = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            PlayerInZone = false;
        }
    }
}
