using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class controls : MonoBehaviour
{
    [Header("Movement")]
    public float movementSpeed;

    public float groundDrag;

    [Header("Ground")]
    public float playerHeight;
    public LayerMask Ground;
    bool grounded;

    [Header("Crounching")]
    public float crounchSpeed;
    public float crounchYScale;
    private float startYScale;

    [Header("KeyBindings")]
    public KeyCode crounchKey = KeyCode.LeftShift;

    public Transform orientation;

    float horizontalInput;
    float verticalInput;

    Vector3 moveDirection;
    Rigidbody rg;

    public MovementState state;

    public enum MovementState
    {
        crounching
    }

    private void Start()
    {
        rg = GetComponent<Rigidbody>();
        rg.freezeRotation = true;

        startYScale = transform.localScale.y;
    }

    private void Update()
    {
        grounded = Physics.Raycast(transform.position, Vector3.down, playerHeight * 0.5f + 0.2f, Ground);

        MyInput();
        SpeedControl();

        if (grounded)
        {
            rg.drag = groundDrag;
        }
        else
        {
            rg.drag = 0;
        }
    }

    private void FixedUpdate()
    {
        MovePlayer();
    }

    private void MyInput()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        if (Input.GetKeyDown(crounchKey))
        {
            transform.localScale = new Vector3(transform.localScale.x, crounchYScale, transform.localScale.z);
            rg.AddForce(Vector3.down * 5f, ForceMode.Impulse);
        }

        if (Input.GetKeyUp(crounchKey))
        {
            transform.localScale = new Vector3(transform.localScale.x, startYScale, transform.localScale.z);
        }
    }

    private void MovePlayer()
    {
        moveDirection = orientation.forward * verticalInput + orientation.right * horizontalInput;
        moveDirection.Normalize();

        rg.AddForce(moveDirection.normalized * movementSpeed * 5f, ForceMode.Force);

    }

    private void SpeedControl()
    {
        Vector3 flatVel = new Vector3(rg.velocity.x, 0f, rg.velocity.y);
        flatVel.Normalize();

        if (flatVel.magnitude > movementSpeed)
        {
            Vector3 limitedVel = flatVel.normalized * movementSpeed;
            rg.velocity = new Vector3(limitedVel.x, rg.velocity.y, limitedVel.z);
        }
    }

    private void StateHandler()
    {
        if (Input.GetKeyDown(crounchKey))
        {
            state = MovementState.crounching;
            movementSpeed = crounchSpeed;
        }
    }
}
