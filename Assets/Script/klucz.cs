using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class klucz : MonoBehaviour
{
    public GameObject key;
    public Transform player;
    public Transform targetObject;

    void Update()
    {
        Vector3 direction = targetObject.position - player.position;
        float distanceToTarget = direction.magnitude;

        if (distanceToTarget <= 2f && Vector3.Dot(direction.normalized, player.forward.normalized) > 0)
        {
            RaycastHit hit;
            if (Physics.Raycast(player.position, direction.normalized, out hit))
            {
                if (hit.transform == targetObject && Input.GetKeyDown(KeyCode.E))
                {
                    Destroy(key);

                }
            }
        }
    }
}
